#!/bin/bash

touch /vagrant/cotton.txt
echo "The fabric of our lives as of $(date +%s)"  >> /vagrant/cotton.txt

sudo mkdir -p /data/db/

sudo cat > /etc/yum.repos.d/mongodb.repo << EOL
[mongodb]
name=MongoDB Repository
baseurl=http://downloads-distro.mongodb.org/repo/redhat/os/x86_64/
gpgcheck=0
enabled=1
EOL

cat /etc/yum.repos.d/mongodb.repo
sudo yum -y update

sudo yum install -y mongodb-org
sudo service mongod start
sudo chkconfig mongod on
sudo tail /var/log/mongodb/mongod.log
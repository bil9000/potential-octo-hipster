# potential-octo-hipster
##Vagrant Centos 6.5 Mongo DB Install
I think you may just need to clone the distro, `cd` into the directory and run:

`vagrant up`

It runs the vagrant box *chef/centos-6.5* -- so if you don't have it installed -- it should go out and pull it down for you like magic.
That could take a while.

If you have to download it, I would go and get a sandwich or something. Because the updates alone and the downloading of mongo are slow. I want to box this thing up and share it so you don't have to download everything every time. 

That is and **area of further research** as they like to say.  Meaning, "one of these days, I'll get around to it; but jeez it would be nice if somebody else in the FOSS world did it for me". 
